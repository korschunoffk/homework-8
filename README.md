# ДЗ 8, сборка своего пакета и публикация его в своем репозитории

# 1. Сборка Своего пакета.

Создаем пользователя, группу для сборки и временную директорию 
```
useradd builder
groupadd builder
mkdir  /opt/lib
cd /opt/lib
```
Устанавливаем необходимые зависимости для сборки пакета 
```
yum install redhat-lsb-core wget rpmdevtools rpm-build createrepo yum-utils  gcc -y

```

Скачиваем SRPM пакет и распаковываем его перед сборкой, появится папка rpmbuild в домашней директории текущего пользователя (root)
```
wget https://nginx.org/packages/centos/7/SRPMS/nginx-1.14.1-1.el7_4.ngx.src.rpm 
rpm -i nginx-1.14.1-1.el7_4.ngx.src.rpm 
```
  
скачиваем Openssl для включения в пакет nginx, распаковываем его

```
wget https://www.openssl.org/source/old/1.1.1/openssl-1.1.1a.tar.gz
tar -xvf openssl-1.1.1a.tar.gz
```
Устанавливаем зависимости, необходимые непосредственно для nginx
`yum-builddep /root/rpmbuild/SPECS/nginx.spec -y #dependencies`


Добавляем в конфиг nginx openssl (указываем путь до папки с разархивированным ранее openssl) и запускаем процесс сборки

```
sed -i "s|--with-debug|--with-debug --with-openssl=/opt/lib/openssl-1.1.1a|g" /root/rpmbuild/SPECS/nginx.spec
rpmbuild -bb /root/rpmbuild/SPECS/nginx.spec #build
```
# 2. Создание своего репозитория

Устанавливаем только что собранный пакет с openssl и стартуем демона

```
yum localinstall -y /root/rpmbuild/RPMS/x86_64/nginx-1.14.1-1.el7_4.ngx.x86_64.rpm 
systemctl start nginx
```

Создаем папку, где будут лежать наши пакеты (nginx и percona), в директории nginx, что бы был доступ через web (и в дальнейшем для repo-файла)
```
mkdir /usr/share/nginx/html/repo
cp /root/rpmbuild/RPMS/x86_64/nginx-1.14.1-1.el7_4.ngx.x86_64.rpm /usr/share/nginx/html/repo/
wget http://www.percona.com/downloads/percona-release/redhat/0.1-6/percona-release-0.1-6.noarch.rpm -O /usr/share/nginx/html/repo/percona-release-0.1-6.noarch.rpm
```

Создаем репозиторий(перечитываем содержание папки и создаем репо. (необходимо запускать при каждом новом пакете)
`createrepo /usr/share/nginx/html/repo/`

Включаем автоиндексацию папок в nginx , проверяем корректность кофига и перезапускаем сервис
```
sed -i "s|index  index.html index.htm;|index  index.html index.htm; \n autoindex on;|g" /etc/nginx/conf.d/default.conf

nginx -t
nginx -s reload
```


Создаем файл repo (для управления через yum)
```
cat <<EOT > /etc/yum.repos.d/otus.repo
[otus]
name=otus-linux
baseurl=http://localhost/repo #тут адрес нашего nginx
gpgcheck=0
enabled=1
EOT
```

`yum-config-manager --enable otus` #на всякий включаем репозиторий

Устанавливаем Percona и удаляем временные файлы.
```
yum install percona-release -y
rm -rf /opt/lib
```

