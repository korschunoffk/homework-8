#!/bin/bash


useradd builder
groupadd builder

mkdir  /opt/lib

yum install redhat-lsb-core wget rpmdevtools rpm-build createrepo yum-utils  gcc -y

cd /opt/lib

wget https://nginx.org/packages/centos/7/SRPMS/nginx-1.14.1-1.el7_4.ngx.src.rpm 
rpm -i nginx-1.14.1-1.el7_4.ngx.src.rpm

wget https://www.openssl.org/source/old/1.1.1/openssl-1.1.1a.tar.gz
tar -xvf openssl-1.1.1a.tar.gz

yum-builddep /root/rpmbuild/SPECS/nginx.spec -y #dependencies



sed -i "s|--with-debug|--with-debug --with-openssl=/opt/lib/openssl-1.1.1a|g" /root/rpmbuild/SPECS/nginx.spec

rpmbuild -bb /root/rpmbuild/SPECS/nginx.spec #build

yum localinstall -y /root/rpmbuild/RPMS/x86_64/nginx-1.14.1-1.el7_4.ngx.x86_64.rpm 

systemctl start nginx

mkdir /usr/share/nginx/html/repo

cp /root/rpmbuild/RPMS/x86_64/nginx-1.14.1-1.el7_4.ngx.x86_64.rpm /usr/share/nginx/html/repo/

wget http://www.percona.com/downloads/percona-release/redhat/0.1-6/percona-release-0.1-6.noarch.rpm -O /usr/share/nginx/html/repo/percona-release-0.1-6.noarch.rpm

createrepo /usr/share/nginx/html/repo/

sed -i "s|index  index.html index.htm;|index  index.html index.htm; \n autoindex on;|g" /etc/nginx/conf.d/default.conf

nginx -t
nginx -s reload


cat <<EOT > /etc/yum.repos.d/otus.repo
[otus]
name=otus-linux
baseurl=http://localhost/repo
gpgcheck=0
enabled=1
EOT

yum-config-manager --enable otus

yum install percona-release -y
rm -rf /opt/lib






